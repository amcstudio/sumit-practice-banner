'use strict';
~ function () {
	var $ = TweenMax,
		ad = document.getElementById('mainContent');

	window.init = function () {
		play();
	}

	function play() {

		createStrip();
		var tl = new TimelineMax();

		    tl.to('#stripBg', .7, { x: -260, y: 260, ease: Sine.easeOut })
			.to('.sprite', .8, { x: 0, ease: Sine.easeOut })

			.to('.sprite', .5, { x: 142, ease: Sine.easeOut }, '+=1.5 ')
			.to('#stripBg', .5, { x: -360, ease: Sine.easeOut }, '-=.5 ')

			.to('.sprite', .5, { x: 285, ease: Sine.easeOut }, '+=1.5 ')
			.to('#stripBg', .5, { x: -460, ease: Sine.easeOut }, '-=.5 ')
			.to('#mainContent', .5, { backgroundColor: '#c12841', ease: Sine.easeOut }, '-=.5 ')

		    .to('.sprite', .5,{ x:430, ease:Sine.easeOut}, '+=1 ')
		    .to('#stripBg', .5, { x: -560, ease: Sine.easeOut }, '-=.5 ')
		    .to('#mainContent', .5, { backgroundColor: '#cd2b30', ease: Sine.easeOut }, '-=.5 ')

		  .to('.sprite', .5,{ x:589, ease:Sine.easeOut}, '+=1 ')
		  .to('#stripBg', .5, { x: -660, ease: Sine.easeOut }, '-=.5 ')
		  .to('#mainContent', .5, { backgroundColor: '#eb1f40', ease: Sine.easeOut }, '-=.5 ')


		//   .to('.sprite', .5,{ x:755, ease:Sine.easeOut}, '+=1 ')
		//   .to('.sprite', .5,{ x:890, ease:Sine.easeOut}, '+=1 ')
		//   .to('.img', .5,{ opacity:0, ease:Sine.easeOut}, '-=.5 ')

	}
	function createStrip() {
		var newleft = 0;
		for (var i = 0; i < 200; i++) {
			newleft += 31;
			var stripBg = document.getElementById('stripBg'),
				strip = document.createElement('div');
			strip.setAttribute('class', 'strip');
			strip.style.left = newleft + "px";
			stripBg.appendChild(strip);
		}
	}

}();
